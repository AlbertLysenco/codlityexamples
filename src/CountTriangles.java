import java.util.Arrays;

// 100%
public class CountTriangles {
    public static void main(String[] args) {
        int[] A = {10, 2, 5, 1, 8, 12};
        CountTriangles countTriangles = new CountTriangles();
        System.out.println(countTriangles.solution(A));
    }

    public int solution(int[] A) {
        int result = 0;

//        for (int P = 0; P < A.length; P++) {
//            for (int Q = P + 1; Q < A.length; Q++) {
//                for (int R = Q + 1; R < A.length; R++) {
//                    if (A[P]+A[Q] > A[R] && A[Q]+A[R] > A[P] && A[R]+A[P] > A[Q]) {
//                        result++;
//                    }
//                }
//            }
//        }

        Arrays.sort(A);

        int R = 0;
        for (int P = 0; P < A.length - 2; P++) {
            R = 0;
            for (int Q = P + 1; Q < A.length - 1; Q++) {
                while (R < A.length && A[P] + A[Q] > A[R]) {
                    R++;
                }
                result += R - Q - 1;
            }
        }
        return result;
    }
}

