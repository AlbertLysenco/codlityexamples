
// 100%
public class TreeHeight {

    static class Tree {
        public int x;
        public Tree l;
        public Tree r;
    }

    public int solution(Tree T) {
        int t1l = 0;
        int t2l = 0;

        if (T.l != null) {
            t1l = 1 + solution(T.l);
        }

        if (T.r != null) {
            t2l = 1 + solution(T.r);
        }

        return Math.max(t1l, t2l);
    }
}
