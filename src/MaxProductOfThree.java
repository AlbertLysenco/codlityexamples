import java.util.Arrays;

//100%
public class MaxProductOfThree {
    public static void main(String[] args) {
        int[] A = {-3, 1, 2, -2, 5, 6};
        MaxProductOfThree maxProductOfThree = new MaxProductOfThree();
        System.out.println(maxProductOfThree.solution(A));
    }

    public int solution(int[] A) {

        Arrays.sort(A);
        int right = A[A.length-1] * A[A.length-2] * A[A.length-3];
        int left = A[A.length-1] * A[0] * A[1];

        return left > right ? left : right;
    }
}
