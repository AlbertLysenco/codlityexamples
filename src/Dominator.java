

// 100%
public class Dominator {
    public static void main(String[] args) {
        int[] A = {3, 4, 3, 2, 3, -1, 3, 3};
        Dominator dominator = new Dominator();
        System.out.println(dominator.solution(A));
    }

    public int solution(int[] A) {
        int size = 0;
        int value = 0;
        int candidate = -1;
        int leader = -1;
        int count = 0;

        for (int i = 0; i < A.length; i++) {
            if (size == 0) {
                size++;
                value = A[i];
            } else {
                if (value != A[i]) {
                    size--;
                } else {
                    size++;
                }
            }
        }

        if (size > 0) {
            candidate = value;
        }

        for (int i = 0; i < A.length; i++) {
            if (A[i] == candidate) {
                count++;
            }
        }
        if (count > A.length / 2) {
            leader = candidate;
        }

        for (int i = 0; i < A.length; i++) {
            if (A[i] == leader) {
                return i;
            }
        }
        return -1;
    }
}
