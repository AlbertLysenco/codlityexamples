

// 70%
public class ArrayInversionCount {

    public static void main(String[] arfs) {
        int[] testArray = {-1, 6, 3, 4, 7, 4};

        ArrayInversionCount arrayInversionCount = new ArrayInversionCount();
        System.out.println(arrayInversionCount.solution(testArray));
    }
    public int solution(int[] A) {
    	if (A.length == 0 || A.length == 1) {
            return 0;
        }

        int result = 0;

        for (int i = 0; i < A.length; i++) {
            for (int j = i + 1; j < A.length; j++) {
                if (i < j && A[i] > A[j]) {
                    result++;
                }
            }
        }

        if (result >= 1000000000) {
            return -1;
        }

        return result;
    }
}