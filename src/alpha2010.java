
// 100%
public class alpha2010 {
    public static void main(String[] args) {
        int[] A = {2, 2, 1, 0, 1};
        alpha2010 alpha2010 = new alpha2010();
        System.out.println(alpha2010.solution(A));
    }

    public int solution(int[] A) {
        int index = 0;

        boolean[] bitMap = new boolean[A.length];

        for (int i = 0; i < A.length; i++) {
            if (!bitMap[A[i]]) {
                bitMap[A[i]] = true;
                index = i;
            }
        }

        return index;
    }
}
