import java.util.ArrayList;
import java.util.LinkedList;

// 100%
public class Brackets {
    public static void main(String[] args) {
        Brackets brackets = new Brackets();
        System.out.println(brackets.solution("{[()()]}"));
    }

    public int solution(String S) {
        LinkedList<Character> stack = new LinkedList<Character>();
        

        for (int i = 0; i < S.length(); i++) {
            char c = S.charAt(i);

            if (c == '{' || c == '[' || c == '(') {
                stack.push(c);
            } else {
                if (stack.isEmpty()) {
                    return 0;
                }

                char corresponding = stack.pop();

                if (c == ')' && corresponding != '(') {
                    return 0;
                }

                if (c == ']' && corresponding != '[') {
                    return 0;
                }

                if (c == '}' && corresponding != '{') {
                    return 0;
                }

            }
        }

        return stack.isEmpty() ? 1 : 0;
    }
}
