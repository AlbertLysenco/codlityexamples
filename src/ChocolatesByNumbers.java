
// 100%
public class ChocolatesByNumbers {
    public static void main(String[] args) {
        int N = 10;
        int M = 4;
        ChocolatesByNumbers chocolatesByNumbers = new ChocolatesByNumbers();
        System.out.println(chocolatesByNumbers.solution(N, M));
    }

    public int solution(int N, int M) {
        int R = N;

        while (M != 0) {
            int tmp = N % M;
            N = M;
            M = tmp;
        }

        return R / N;
    }
}
