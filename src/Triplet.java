

//50 need O(N*log(n)) but O(n3)
public class Triplet {
	public static void main(String[] args) {
		int[] testArray = {10, 2, 5, 1, 8, 20};
		
		Triplet triplet = new Triplet();
		System.out.println(triplet.solution(testArray));
	}

	public int solution(int[] A) {
		int result = 0;
		
		for (int i = 0; i < A.length; i++) {
			for (int j = i + 1; j < A.length; j++) {
				for (int k = j  + 1; k < A.length; k++) {
					if (A[i] + A[j] > A[k] && A[j] + A[k] > A[i] && A[k] + A[i] > A[j]) {
						result = 1;
						System.out.println(i + ", " + j + ", " + k);
						break;
					}
				}
			}
		}
		
		
		return result;
	}
}
