import java.util.HashSet;
import java.util.Set;

// 100%
public class AbsDistinct {
    public static void main(String[] args) {
        int[] A = {-5, -3, -1, 0, 3, 6};
        AbsDistinct absDistinct = new AbsDistinct();
        System.out.println(absDistinct.solution(A));
    }

    public int solution(int[] A) {

        Set<Integer> set = new HashSet();

        for (int i = 0; i < A.length; i++) {
            if (A[i] < 0) {
                A[i] =  Math.abs(A[i]);
            }
        }

        for (int a : A) {
            set.add(a);
        }

        return set.size();
    }
}
