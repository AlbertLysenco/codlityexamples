

// 100%
public class FrogRiverOne {

    public static void main(String[] args) {
        int[] A = {1, 2, 1, 4, 2, 3, 5, 4};
        FrogRiverOne frogRiverOne = new FrogRiverOne();
        System.out.println(frogRiverOne.solution(5, A));
    }

    public int solution(int X, int[] A) {
        int steps = X;
        boolean[] bitMap = new boolean[X + 1];

        for (int i = 0; i < A.length; i++) {
            if(!bitMap[A[i]]) {
                bitMap[A[i]] = true;
                steps--;
            }
            if (steps == 0) return i;
        }

        return -1;
    }

}
