

// 100%
public class FrogJmp {

    public static void main(String[] args) {
        FrogJmp frogJmp = new FrogJmp();
        System.out.println(frogJmp.solution(10, 85, 30));
    }

    public int solution(int X, int Y, int D) {
        int length = Y - X;
        int result = length / D;

        if (length%D != 0) {
            return result + 1;
        }

        return result;
    }

}
