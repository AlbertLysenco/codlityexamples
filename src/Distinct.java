
import java.util.HashSet;
import java.util.Set;

//100
public class Distinct {

	public static void main(String[] args) {
		int[] testArray = {2, 1, 1, 2, 3, 1};
		Distinct distinct = new Distinct();
		System.out.println(distinct.solution(testArray));
	}

	public int solution(int[] A) {
		
		Set<Integer> set = new HashSet<Integer>();
		
		for (int i : A) {
			set.add(i);
		}
		
		return set.size();
    }
}
