
public class EquiLeader {
//100
    public static void main(String[] args) {
        int[] testArray = {4, 3, 4, 4, 4, 2};

        EquiLeader equiLeader = new EquiLeader();
        System.out.println(equiLeader.solution(testArray));
    }

    public int solution(int[] A) {
    	int equivalent = 0;
        int size = 0;
        int value = 0;
        int candidate = -1;
        int leader = -1;
        int count = 0;

        for (int i = 0; i < A.length; i++) {
            if (size == 0) {
                size++;
                value = A[i];
            } else {
                if (value != A[i]) {
                    size--;
                } else {
                    size++;
                }
            }
        }

        if (size > 0) {
            candidate = value;
        }

        for (int i = 0; i < A.length; i++) {
            if (A[i] == candidate) {
                count++;
            }
        }
        if (count > A.length / 2) {
            leader = candidate;
        }

        int totalLeaders = 0;
        for(int i : A){
            if(i == leader) totalLeaders++;
        }

        int leaderLefCount = 0;
        for (int i = 0; i < A.length; i++) {
            if (A[i] == leader) {
                leaderLefCount++;
            }
            int leaderRightCount = totalLeaders - leaderLefCount;
            if (leaderLefCount > (i+1)/2 && leaderRightCount > (A.length-i-1)/2) {
                equivalent++;
            }
        }
        return equivalent;
    }
}
