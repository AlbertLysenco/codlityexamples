import java.util.Arrays;

// 100%
public class PermMissingElem {

    public static void main(String[] args) {
    	int[] A = {2, 4, 3, 1};
        PermMissingElem  permMissingElem = new PermMissingElem();
        System.out.println(permMissingElem.solution(A));
    }

    public int solution(int[] A) {
// First way
//        Arrays.sort(A);
//        int testRes = 1;
//
//        for (int i = 0; i < A.length ; i++) {
//            if (A[i] == testRes) {
//                testRes++;
//            }
//            else break;
//        }
//    	return testResult;
    	
//    	Second way
    	long factSum = 0;
    	
    	for (int i = 0; i < A.length; i++) {
    		factSum += A[i];
    	}
    	
    	long teoreticSum = (long) (A.length +1)*(A.length + 2)/2;
    	
        return (int) (teoreticSum - factSum);
    }

}
