
// 100%
public class BinaryGap {
    public static void main(String[] args) {
        int N = 1041;
        BinaryGap binaryGap = new BinaryGap();
        System.out.println(binaryGap.solution(N));
    }

    public int solution(int N) {
        boolean foundOne = false;
        int maxZeros = 0;
        int zerosCount = 0;

        while (N > 0) {
            if (N % 2 == 1) {
                foundOne = true;

                if (zerosCount > maxZeros) {
                    maxZeros = zerosCount;
                }
                zerosCount = 0;
            } else {
                if (foundOne) {
                    zerosCount++;
                }
            }
            N /= 2;
        }

        return maxZeros;
    }
}
