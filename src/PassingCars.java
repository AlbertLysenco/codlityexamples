/**
 * Created by OKlochko on 17.04.2014.
 */

// 100%
public class PassingCars {

    public static void main(String[] args) {
        int[] testArray = { 0, 1, 0, 1, 1 };

        PassingCars passingCars = new PassingCars();
        System.out.println(passingCars.solution(testArray));

    }

    public int solution(int[] A) {
        int result = 0;
        int zCount = 0;

        for (int i : A) {
            if (i == 1) {
                result += zCount;
            } else {
                zCount++;
            }
        }

        if (result > 100_000_000 || result < 0) {
            return -1;
        }

        return result;
    }

}
