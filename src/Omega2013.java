

// 100%
public class Omega2013 {

    public static void main(String[] args) {
        int[] testArrayWell = new int[]{5, 6, 4, 3, 6, 2, 3};
        int[] testArrayDrop = new int[]{2, 3, 5, 2, 4};

        Omega2013 omega2013 = new Omega2013();
        System.out.println(omega2013.solution(testArrayWell, testArrayDrop));
    }

    public int solution(int[] A, int[] B) {
    	int N = A.length;
        int result = 0;

        while (result < B.length && N > 0) {
            int k = -1;
            while (k + 1 < N && B[result] <= A[k + 1]) {
                k++;
            }
            if (k > -1) {
                result++;
            }
            N = k;
        }
        return result;
    }

}
