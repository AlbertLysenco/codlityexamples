import java.util.Arrays;



// 100%
public class PermCheck {

    public static void main(String[] args) {
        int[] A = {4, 1,  2};
        PermCheck permCheck = new PermCheck();
        System.out.println(permCheck.solution(A));
    }

    public int solution(int[] A) {
        int result = 0;

        Arrays.sort(A);
        int testRes = 1;

        for (int i = 0; i < A.length; i++) {
            if (A[i] == testRes) {
                testRes++;
            } else {
                result = testRes;
            }
        }

        if (result == 0) {
            return 1;
        }

        return 0;
    }

}