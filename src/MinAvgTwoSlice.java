public class MinAvgTwoSlice {
//100
	public static void main(String[] args) {
		int[] testArray = { 4, 2, 2, 5, 1, 5, 8 };
		MinAvgTwoSlice minAvgTwoSlice = new MinAvgTwoSlice();

		System.out.println(minAvgTwoSlice.solution(testArray));
	}

	public int solution(int[] A) {

		int index = -1;

		double avarage3 = 0.0;
		double avarage2 = 0.0;

		double minAva = Double.MAX_VALUE;

		for (int i = 0; i < (A.length - 2); i++) {

			avarage3 = (A[i] + A[i + 1] + A[i + 2]) / 3.0;
			avarage2 = (A[i] + A[i + 1]) / 2.0;

			if (minAva > avarage3) {
				minAva = avarage3;
				index = i;
			}

			if (minAva > avarage2) {
				minAva = avarage2;
				index = i;
			}

		}

		avarage2 = (A[A.length - 2] + A[A.length - 1]) / 2.0;

		if (minAva > avarage2) {
			index = A.length - 2;
		}
		return index;
	}

}
