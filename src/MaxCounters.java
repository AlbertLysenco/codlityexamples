import java.util.Arrays;

//77%
public class MaxCounters {

    public static void main(String[] args) {
        int[] A = {1, 2, 1, 4, 2, 3, 5, 4};
        MaxCounters maxCounters = new MaxCounters();
        System.out.println(maxCounters.solution(5, A));
    }

    public int[] solution(int N, int[] A) {
		int[] result = new int[N];
		int maxCoun = 0;

		for (int i = 0; i < A.length; i++) {
			int operation = A[i];

			if (operation <= N && operation >= 1) {
				result[operation - 1]++;
				if (maxCoun < result[operation - 1]) {
					maxCoun = result[operation - 1];
				}

			} else {
				Arrays.fill(result, maxCoun);
			}

		}

		return result;
    }
}
