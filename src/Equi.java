
// 100%
public class Equi {
    public static void main(String[] args) {
        int[] A = {-7, 1, 5, 2, -4, 3, 0};
        Equi equi = new Equi();
        System.out.println(equi.solution(A));
    }

    public int solution(int[] A) {
        long sumLeft = 0;
        long sumRight = 0;

        if (A.length == 0) {
            return -1;
        }

        for (int i = 1; i < A.length; i++) {
            sumRight += A[i];
        }

        if (sumLeft == sumRight) {
            return 0;
        }

        for (int P = 1; P < A.length; P++) {
            sumLeft += A[P-1];
            sumRight -= A[P];
            if (sumLeft == sumRight) {
                return P;
            }
        }

        return -1;
    }
}
