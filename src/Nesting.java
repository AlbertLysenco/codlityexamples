import java.util.LinkedList;



// 100%
public class Nesting {
    public static void main(String[] args) {
        Nesting nesting = new Nesting();
        System.out.println(nesting.solution("(()(())())"));
    }

    public int solution(String S) {
        LinkedList<Character> stack = new LinkedList<Character>();

        for (int i = 0; i < S.length(); i++) {
            char c = S.charAt(i);

            if (c == '(') {
                stack.push(c);
            } else {
                if (stack.isEmpty()) {
                    return 0;
                }

                char corresponding = stack.pop();

                if (c == ')' && corresponding != '(') {
                    return 0;
                }

            }
        }

        return stack.isEmpty() ? 1 : 0;
    }
}
