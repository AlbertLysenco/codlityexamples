
// 100%
public class TapeEquilibrium {

	public static void main(String[] args) {
		int[] A = {1000, -1000 };
		TapeEquilibrium tapeEquilibrium = new TapeEquilibrium();
		System.out.println(tapeEquilibrium.solution(A));
	}

	public int solution(int[] A) {
		int result;
		int sumLeft = 0;
		int sumRight = 0;

		for (int i = 1; i < A.length; i++) {
			sumRight += A[i];
		}

		sumLeft = A[0];
		result = Math.abs(sumLeft - sumRight);

		for (int P = 1; P < A.length; P++) {
			if (Math.abs(sumLeft - sumRight) < result) {
				result = Math.abs(sumLeft - sumRight);
			}
			sumLeft += A[P];
			sumRight -= A[P];
		}

		return result;
	}
}
